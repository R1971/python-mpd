Source: python-mpd
Section: python
Priority: optional
Maintainer: mpd maintainers <pkg-mpd-maintainers@lists.alioth.debian.org>
Uploaders:
 Geoffroy Youri Berret <efrim@azylum.org>,
 Simon McVittie <smcv@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 dh-sequence-sphinxdoc,
 python3-all,
 python3-setuptools,
 python3-sphinx,
 python3-sphinxcontrib.devhelp,
 python3-twisted <!nocheck>,
Rules-Requires-Root: no
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/mpd-team/python-mpd.git
Vcs-Browser: https://salsa.debian.org/mpd-team/python-mpd
Homepage: https://github.com/Mic92/python-mpd2
Testsuite: autopkgtest-pkg-python

Package: python-mpd-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Built-Using:
 ${sphinxdoc:Built-Using},
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 ${sphinxdoc:Depends},
Suggests:
 devhelp,
Description: Python MPD client library (documentation)
 Fast MPD (Music Player Daemon) client library written in pure Python.
 It was written to be a replacement for python-mpdclient which is a bit
 outdated and does not perform good in many situations.
 .
 This package contains the documentation.

Package: python3-mpd
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Breaks:
 python3-mpd2 (<< 0.5.5-1~),
Replaces:
 python3-mpd2 (<< 0.5.5-1~),
Description: Python MPD client library (Python 3)
 Fast MPD (Music Player Daemon) client library written in pure Python.
 It was written to be a replacement for python-mpdclient which is a bit
 outdated and does not perform good in many situations.
 .
 This is the Python 3 version of python-mpd2, a fork of the original
 python-mpd.
